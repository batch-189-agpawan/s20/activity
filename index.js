// alert('hello')

let userNumber = Number(prompt("Please provide a number:"))
console.log('The number you provided is ' + userNumber)

for( let number = userNumber ; number > 0; number--){
	
	if(
		number<=50){
		break;
	}
	if(
		number % 10 === 0
		)
	{
		console.log('The number is divisible by 10. Skipping the number')
		continue;
	} 
	if(
		number % 5 === 0){
		console.log(number)
	}
}

let string = "supercalifragilisticexpialidocious";
let consonants = "";
console.log(string);
for (let i = 0; i < string.length; i++){
    if ((string[i] === "a") || 
        (string[i] === "e") ||
        (string[i] === "i") ||
        (string[i] === "o") ||
        (string[i] === "u")
        ){
        continue;
    } else {
        consonants += string[i];
    }
}
console.log(consonants);